//
//  ViewController.swift
//  p2
//
//  Created by Matthew Stromberg on 2/5/16.
//  Copyright © 2016 Matthew Stromberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var numberArray: [UILabel]!
    var array: [[Int]] = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
    let numRows = 4
    let numColumns = 4
        
        
    
    override func viewDidLoad() {
        print("Loaded!");
        addNewNumber()
        addNewNumber()
        for (var x=0; x < numColumns; x++){
                print(array[x])
        }
        super.viewDidLoad()
        var swipedLeft = UISwipeGestureRecognizer(target: self, action: "moveLeft")
        // Do any additional setup after loading the view, typically from a nib.
        swipedLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipedLeft)
        
        var swipedRight = UISwipeGestureRecognizer(target: self, action: "moveRight")
        // Do any additional setup after loading the view, typically from a nib.
        swipedRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipedRight)
        
        var swipedUp = UISwipeGestureRecognizer(target: self, action: "moveUp")
        // Do any additional setup after loading the view, typically from a nib.
        swipedUp.direction = UISwipeGestureRecognizerDirection.Up
        self.view.addGestureRecognizer(swipedUp)
        
        var swipedDown = UISwipeGestureRecognizer(target: self, action: "moveDown")
        // Do any additional setup after loading the view, typically from a nib.
        swipedDown.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipedDown)
    }
/*
    func myButton() {
        let label : UILabel = self.view.viewWithTag(index) as! UILabel
        label.text = day as String
        for(var i = 0; i < numRows*numColumns; i++){
        numberArray[numberArray.startIndex] = "Label changed!";
        }
    }
  */  
    func moveLeft(){
        rotate()
        foobar()
        rotate()
        rotate()
        rotate()
        updateFrontEnd()
    }
    
    func moveRight(){
        rotate()
        rotate()
        rotate()
        foobar()
        updateFrontEnd()
    }
    
    func moveDown(){
        rotate()
        rotate()
        foobar()
        rotate()
        rotate()
        updateFrontEnd()
    }
    
    func moveUp(){
        foobar()
        updateFrontEnd()
    }
    
    func foobar(){
        var t=0,stop=0
        for(var y = 0; y<numColumns; y++){
            for(var x = 0; x<numRows ;x++){
                if(array[y][x] != 0){
                    var z = trigger(array[y],x: t,y: stop)
                    if(z != x){
                        if (array[y][z] == 0){
                            array[y][z] = array[y][x]
//                            numberArray[((4*y)+z)].text = String(array[y][x])

                        }
                        else if(array[y][z] == array[y][x])
                        {
                            array[y][z] = ((array[y][z])*2)
//                            numberArray[((4*y)+z)].text = String(((array[y][z])*2))
                            //SCORE GETS MODIFIED HERE
                            stop = t+1
                        }
                        array[y][x]=0
//                        numberArray[((4*y)+x)].text = String("0")
                    }
                }
            }
        }
//        for(var x=0)
        print("Swiped left!")
        addNewNumber()
        for (var x=0; x < numColumns; x++){
            print(array[x])
        }

    }
    
    func updateFrontEnd(){
        for (var x=0; x < numColumns; x++){
            for(var y=0; y < numRows; y++){
                numberArray[((4*x)+y)].text = String(array[x][y])
            }
        }
    }
    
    func trigger(var localArray: [Int], var x: Int, var y: Int) -> Int{
        if(x==0){
            return 0;
        }
        for(var t = x-1; t >= 0; t--){
            if(localArray[t] != 0){
                if (localArray[t] != localArray[x]){
                    return t+1
                }
                return t
            }
            else{
                if (t == y){
                    return t
                }
                
            }
        }
        return x
    }
    
    func addNewNumber(){
        var x = Int(arc4random_uniform(4))
        var y = Int(arc4random_uniform(4))
        while(array[x][y] != 0){
            x = Int(arc4random_uniform(4))
            y = Int(arc4random_uniform(4))
        }
        array[x][y] = 2
//        numberArray[((4*x)+y)].text = "2"
//        array[Int(arc4random_uniform(4))][Int(arc4random_uniform(4))] = 2
//        array[Int(arc4random_uniform(4))][Int(arc4random_uniform(4))] = 2
        
    }
    
    func rotate(){
        var n = numRows
        for(var i=0; i < (numRows/2); i++){
            for(var j=i; j < (n-i-1); j++){
                var tmp = array[i][j]
                array[i][j] = array[j][n-i-1]
                array[j][n-i-1] = array[n-i-1][n-j-1]
                array[n-j-1][n-j-1] = array[n-j-1][i]
                array[n-j-1][i] = tmp
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}

